/**
 *
 * @param {any} params 通过platforn判断终端类型
 * @returns {number} 1  pc  0 wap
 */
let isPc = ((params) => {
  let r = 0;
  let osType = navigator.platform
  let flags = ['Win32', 'Linux', 'Mac']
  flags.forEach(flag => {
    if (osType.indexOf(flag) !== -1) {
      r = 1
    }
  })

  return r

})(navigator.platform)